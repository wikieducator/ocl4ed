An example [Wordpress](http://wordpress.org/) theme for deploying the Open Content Licensing for Educators ([OCL4Ed](http://wikieducator.org/Practice:OCL4Ed)) course in a Wordpress instance.

This theme is a child theme of the CyberChimps [responsive](https://wordpress.org/themes/responsive) theme.